package com.example.personsrest;

import com.example.personsrest.domain.Person;
import com.example.personsrest.domain.PersonImpl;
import com.example.personsrest.domain.PersonRepository;
import com.example.personsrest.remote.GroupRemote;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PersonService {
    PersonRepository personRepository;
    GroupRemote groupRemote;

    public List<Person> all() {
        return personRepository.findAll();
    }

    public Page<Person> all(String search, int pagenumber, int pagesize) {
        Pageable page = PageRequest.of(pagenumber, pagesize);
        return personRepository.findAllByNameContainingOrCityContaining(search, search, page);
    }

    public Optional<Person> get(String id) {
        return personRepository.findById(id);
    }

    public Person create(String name, String city, int age) {
        PersonImpl person = new PersonImpl(name, city, age);
        return personRepository.save(person);
    }

    public Person update(String id, String name, String city, int age) {
        Person person = personRepository.findById(id).orElse(null);
        person.setName(name);
        person.setCity(city);
        person.setAge(age);
        return personRepository.save(person);
    }

    public Person delete(String id) {
        Person person = personRepository.findById(id).orElse(null);
        personRepository.delete(id);
        return person;
    }

    public Person addGroup(String id, String name) {
        String groupId = groupRemote.createGroup(name);
        Person person = personRepository.findById(id).orElse(null);
        person.addGroup(groupId);
        return personRepository.save(person);
    }

    public String getGroupName(String groupId) {
        return groupRemote.getNameById(groupId);
    }

    public Person removeGroup(String id, String groupId) {
        Person person = personRepository.findById(id).orElse(null);
        person.removeGroup(groupId);
        return personRepository.save(person);
    }
}
