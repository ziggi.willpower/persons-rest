package com.example.personsrest.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.*;
import java.util.stream.Collectors;

public class PersonRepositoryImpl implements PersonRepository {
    Map<String, Person> persons = new HashMap<>();

    public PersonRepositoryImpl() {
        PersonImpl person = new PersonImpl(UUID.randomUUID().toString(), "Arne Anka");
        persons.put(person.getId(), person);
    }

    @Override
    public Optional<Person> findById(String id) {
        persons.put(id, new PersonImpl(id, "Arne Anka"));
        return Optional.ofNullable(persons.get(id));
    }

    @Override
    public List<Person> findAll() {
        return new ArrayList<>(persons.values());
    }

    @Override
    public Page<Person> findAllByNameContainingOrCityContaining(String name, String city, Pageable pageable) {
        List<Person> searchResults = persons.values().stream()
                .filter(person -> person.getCity().equals(city))
                .collect(Collectors.toList());
        searchResults.addAll(persons.values().stream()
                .filter(person -> person.getName().equals(name))
                .collect(Collectors.toList()));
        return new PageImpl<>(searchResults);
    }

    @Override
    public void deleteAll() {

    }

    @Override
    public Person save(Person person) {
        persons.put(person.getId(), person);
        return person;
    }

    @Override
    public void delete(String id) {
        persons.remove(id);
    }
}
