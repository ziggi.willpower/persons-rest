package com.example.personsrest.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PersonImpl implements Person {
    private String id;
    private String name;
    private String city;
    private int age;
    private List<String> groups;

    public PersonImpl(String id, String name) {
        this.id = id;
        this.name = name;
        this.groups = new ArrayList<>();
    }

    public PersonImpl(String name, String city, int age) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.city = city;
        this.age = age;
        this.groups = new ArrayList<>();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void setActive(boolean active) {

    }

    @Override
    public List<String> getGroups() {
        return groups;
    }

    @Override
    public void addGroup(String groupId) {
        groups.add(groupId);
    }

    @Override
    public void removeGroup(String groupId) {
        groups.removeIf(group -> group.equals(groupId));
    }
}
