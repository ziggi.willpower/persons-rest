package com.example.personsrest;

import com.example.personsrest.domain.Person;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/persons")
@AllArgsConstructor
public class PersonsController {
    PersonService personService;

    @GetMapping
    public List<PersonDTO> all() {
        return personService.all().stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping(params = {"search", "pagenumber", "pagesize"})
    public Page<PersonDTO> all(@RequestParam("search") String search,
                               @RequestParam("pagenumber") int pagenumber,
                               @RequestParam("pagesize") int pagesize) {
        List<PersonDTO> personDTOS = personService.all(search, pagenumber, pagesize).stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
        return new PageImpl<>(personDTOS);
    }

    @GetMapping("/{id}")
    public PersonDTO get(@PathVariable("id") String id) {
        return toDTO(personService.get(id).orElse(null));
    }

    @PostMapping
    public PersonDTO create(@RequestBody CreatePerson createPerson) {
        return toDTO(personService.create(
                createPerson.getName(),
                createPerson.getCity(),
                createPerson.getAge()));
    }

    @PutMapping("/{id}")
    public PersonDTO update(@PathVariable("id") String id,
                            @RequestBody UpdatePerson updatePerson) {
        return toDTO(personService.update(
                id,
                updatePerson.getName(),
                updatePerson.getCity(),
                updatePerson.getAge()
        ));
    }

    @DeleteMapping("/{id}")
    public PersonDTO delete(@PathVariable("id") String id) {
        return toDTO(personService.delete(id));
    }

    @PutMapping("/{id}/addGroup/{name}")
    public PersonDTO addGroup(@PathVariable("id") String id, @PathVariable("name") String name) {
        return toDTO(personService.addGroup(id, name));
    }

    @DeleteMapping("/{id}/removeGroup/{groupId}")
    public PersonDTO removeGroup(@PathVariable("id") String id, @PathVariable("groupId") String groupId) {
        return toDTO(personService.removeGroup(id, groupId));
    }

    private PersonDTO toDTO(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getCity(),
                person.getAge(),
                person.getGroups().stream()
                        .map(groupId -> personService.getGroupName(groupId))
                        .collect(Collectors.toList())
        );
    }

    @Value
    private static class PersonDTO {
        String id;
        String name;
        String city;
        int age;
        List<String> groups;
    }

    @Value
    private static class CreatePerson {
        String name;
        String city;
        int age;
    }

    @Value
    private static class UpdatePerson {
        String name;
        String city;
        int age;
    }

}
