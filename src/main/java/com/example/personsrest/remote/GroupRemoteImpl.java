package com.example.personsrest.remote;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.springframework.web.reactive.function.client.WebClient;

public class GroupRemoteImpl implements GroupRemote {
    private KeyCloakToken keyCloakToken;

    public GroupRemoteImpl() {
        this.keyCloakToken = KeyCloakToken.acquire(
                "https://iam.sensera.se/",
                "test",
                "group-api",
                "user",
                "djnJnPf7VCQvp3Fc"
        ).block();
    }

    @Override
    public String getNameById(String groupId) {
        WebClient webClient = WebClient.create();

        Group group = webClient
                .get()
                .uri("https://groups.edu.sensera.se/api/groups/" + groupId)
                .header("Authorization","Bearer "+ keyCloakToken.getAccessToken())
                .retrieve()
                .bodyToMono(Group.class)
                .block();

        return group.getName();
    }

    @Override
    public String createGroup(String name) {
        WebClient webClient = WebClient.create();
        CreateGroup createGroup = new CreateGroup(name);

        Group group = webClient
                .post()
                .uri("https://groups.edu.sensera.se/api/groups")
                .header("Authorization","Bearer "+ keyCloakToken.getAccessToken())
                .bodyValue(createGroup)
                .retrieve()
                .bodyToMono(Group.class)
                .block();

        return group.getId();
    }

    @Override
    public String removeGroup(String name) {
        return null;
    }

    @Value
    static class Group {
        String id;
        String name;

        @JsonCreator
        public Group(
                @JsonProperty("id") String id,
                @JsonProperty("name") String name) {
            this.id = id;
            this.name = name;
        }
    }

    @Value
    static class CreateGroup {
        String name;
    }
}
